#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 24 12:38:24 2017

@author: tsmuser
"""

import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt

#objective function
def gauss2d(xf,x0,y0,a,b):
    """Compute 2d gaussian function, exp(-a*(x-x0)^2-b*(y-y0)^2)
    x = xf[0], y = xf[1]
    """
    x = xf[0]
    y = xf[1]

    return -np.exp(-a*(x-x0)**2 - b*(y-y0)**2)


#gradient of objective function
def gauss2d_grad(xf,x0,y0,a,b):
    """Compute gradietn of 2d gaussian function
    defined in gauss2d. Returns two-element tuple
    containing (df/dx,df/dy)
    """

    #compute function
    f = gauss2d(xf,x0,y0,a,b)

    x = xf[0]
    y = xf[1]

    return np.array([-2.0*a*(x-x0)*f,-2.0*b*(y-y0)*f])

def econstraint(U):
    """U is a two-element tuple
       containing x and y"""
    return U[1]-np.sin(U[0])
   
xguess = (2,np.sin(2))
parameters = (1.0,3.0,1.0,1.0) #x0,y0,a,b
cons = ({'type':'eq','fun':econstraint}) #equality constraint

result = minimize(gauss2d,xguess,args=parameters, method='SLSQP', constraints=cons)
x,y=result.x[0],result.x[1] #extract location of optimum from result

#display objective
def display_gauss2d(args):
    """display objective functions and curves indicating 
    inequality (red) and equality (green) constraints"""

    from matplotlib import colors

    x0,y0,a,b=args
    
    x = np.linspace(-5+x0,5+x0,101)
    y = np.linspace(-5+y0,5+y0,101)
    
    xg,yg=np.meshgrid(x,y)
    xf = (xg,yg)
    
    
    
    f = gauss2d(xf,x0,y0,a,b)
    
    plt.figure()
    V = np.logspace(-6,0,20)
   # plt.contour(xg,yg,-f,V,norm=colors.LogNorm())
    plt.contour(xg,yg,-f,V,norm=colors.LogNorm())
    plt.plot(x,np.sin(x),'g--')
    plt.axis([x0-5,x0+5,y0-5,y0+5])
    plt.legend(('inequality constraint','equality constraint'),loc='best')
    plt.xlabel('x')
    plt.ylabel('y')
    
display_gauss2d(parameters)
plt.plot(x,y,'r*',markersize=8)