# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np

G=nx.barabasi_albert_graph(500,4)
nx.draw(G,node_size=6)
plt.savefig('Barabasi-Albert Graph')

G2=nx.barabasi_albert_graph(5000,4)
G3=nx.gnp_random_graph(5000,0.002)

plt.loglog(nx.degree_histogram(G2))
plt.hold
plt.loglog(nx.degree_histogram(G3))
plt.savefig('Degree Distributions')

CG2=nx.cluster.average_clustering(G2)
CG3=nx.cluster.average_clustering(G3)
diffC=np.abs(CG2-CG3)
print(diffC)